//Author:  Maria Luisa Ortiz Carstensen
//Project: FSM Package
//Module:  Definitions for FSM and Counter
//Date:	  12/03/2023


package FSM_Counter_pkg;

localparam count = 12;
localparam FSM = 3;

typedef reg [count:0] counter_t;


// States for Transmission //
typedef enum reg [FSM-1:0]
{
	Tx_IDLE = 3'b000;
	Tx_START = 3'b001;
	Tx_DATA = 3'b010;
	Tx_PARITY = 3'b011;
	Tx_STOP  = 3'b111;
} Tx_States;

// States for Reception //
typedef enum reg [FSM-1:0]
{
	Rx_IDLE = 3'b000;
	Rx_START = 3'b001;
	Rx_DATA = 3'b010;
	Rx_PARITY = 3'b011;
	Rx_STOP  = 3'b111;
} Rx_States;


endpackage  

