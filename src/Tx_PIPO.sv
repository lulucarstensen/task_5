//Coder:          Abisai Ramirez Perez
//Description:    This module defines a parametric pipo register


module Tx_PIPO #(
parameter reg_size = 8
) (
input  bit                clk,
input  bit                rst,
input  logic              enb,
input  logic [reg_size-1:0]  pipo_inp,
output logic [reg_size-1:0]  pipo_out
);

logic [reg_size-1:0]      pipo_rgstr_r;

always_ff@(posedge clk or negedge rst) begin: pipo_label
    if(!rst)
        pipo_rgstr_r  <= '0;
    else if (enb)
        pipo_rgstr_r  <= pipo_inp;
end:pipo_label

assign pipo_out = pipo_rgstr_r;

endmodule
