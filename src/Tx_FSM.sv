//Author:  Maria Luisa Ortiz Carstensen
//Project: UART ECHO
//Module:  Tx FSM
//Date:	  12/03/2023

import FSM_Counter_pkg::*;

module Tx_FSM
(
	input clk,
	input enb,
	input rst,
	input tick_counter;
	input logic ovf,
	input logic parity;
	input [DW-1:0] tx_inp,
	input tx_start,
	output l_s,
	output logic tx_out,
	output logic tx_done
);


Tx_States Tx_States;
Tx_States tx_nxt_state;


always_ff @(posedge clk or negedge rst) begin //Clock and Reset Dependency Logic
	if(!rst) begin
		Tx_States <= Tx_IDLE; //Goes to Tx_IDLE Tx_States if reset is active
	end 
	else begin
		Tx_States <= tx_nxt_state; //Otherwise, changes to following Tx_States
	end
end


always_comb begin //Input Dependency Logic
	case (Tx_States)
	
		Tx_IDLE: begin
			if(tx_start)
				tx_nxt_state <= Tx_START;//Tx_IDLE is activated and goes to START
			else 
				tx_nxt_state <= Tx_IDLE;//Otherwise, it stays in Tx_IDLE
		end
		
		Tx_START: begin
			tx_nxt_state <= Tx_DATA;
		end
		
		Tx_DATA: begin
			if(tick_counter == DW)
				tx_nxt_state <= Tx_PARITY;
			else 
				tx_nxt_state <= Tx_DATA;//Still operating under DW, so still processing			
		end
		
		Tx_PARITY: begin //Checks parity bit
			if(parity) 
				tx_nxt_state <= Tx_STOP;
			else
				tx_nxt_state <= Tx_PARITY;
		end
		
		default: begin
			tx_nxt_state <= Tx_STOP; //Clears and starts over
		end
		
	endcase
	
end
			
			
always_comb begin //Output Dependency Logic
		case (Tx_States)
			Tx_IDLE: begin
				tx_out = 1;
				tx_done = 1;
			end
			
			Tx_START: begin
				tx_out = 0;
				tx_done = 1;			
			end
			
			Tx_DATA: begin
				tx_out = 1;
				tx_done = 1;			
			end
			
			Tx_PARITY: begin
				tx_out = 1;
				tx_done = 1;			
			end
			
			default: begin //STOP
				tx_out = 1;
				tx_done = 1;		
			end
		
		endcase
		
	end
	
endmodule

