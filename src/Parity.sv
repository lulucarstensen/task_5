//Author:  Maria Luisa Ortiz Carstensen
//Project: UART ECHO
//Module:  Parity
//Date:	  12/03/2023

module Parity (
	input [W_PIPO-1:0] inp_par;
	output logic parity_bit; 
);

assign parity_bit = ^(inp_par); //We use an xor reduction to check if the odd parity bit is 1 or 0

endmodule 