//Module: PISO for Tx
//Date:	  12/03/2023

import regs_pkg::*;


module Tx_PISO_MSB #(parameter W_PISO_MSB = regs_pkg::W_PISO_MSB)
(
	input bit                     clk,    // Clock
	input bit                     rst,    // asynchronous reset low active 
	input logic                   enb,    // Enable
	input logic                   l_s,    // load or shift
	input logic [W_PISO_MSB-1:0]  Tx_inp,    // data input
   output logic                  Tx_out     // Serial output
);

typedef logic [W_PISO_MSB-1:0]   piso_data_t;

piso_data_t     rgstr_r     ;
piso_data_t     rgstr_nxt   ;

// Combinational module
always_comb begin
    if (l_s)
        rgstr_nxt  = Tx_inp;
    else 
        rgstr_nxt  = {rgstr_r[W_PISO_MSB-2:0], rgstr_r[W_PISO_MSB-1]};
   //      rgstr_nxt  = rgstr_r<<1;
end

always_ff@(posedge clk or negedge rst) begin: piso_label
       if(!rst)
        rgstr_r     <= '0           ;
    else if (enb) begin
        rgstr_r     <= rgstr_nxt    ;
    end
end: piso_label

assign Tx_out  = rgstr_r[W_PISO_MSB-1];    // MSB bit is the first to leave
//######################################################################################################
//TODO: try to design a piso register, where the LSB bit leave the register first and then the LSB bit+1.
//######################################################################################################
endmodule
