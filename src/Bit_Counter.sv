 //Author:  Maria Luisa Ortiz Carstensen
//Project: UART ECHO
//Module:  Bit Counter
//Date:	  12/03/2023

import FSM_Counter_pkg::*;

module Bit_Counter #(parameter TICK_LIM = 11)
(
	input clk,
	input rst,
	input enb,
	output logic ovf,
	output logic tick_counter // For TB
	
);

//Variables to aid data transfer
counter_t count_temp;
counter_t count_nxt;//Counter add

logic overflow;


always_ff@(posedge clk, negedge rst) begin: ticks_counter

	if (!rst)
		count_temp <= '0; // Set counter to 0 if rst is active
	else if (enb)
		if(count_nxt >= TICK_LIM)
			count_temp <= '0; // If we reached ovf, we set counter to 0
		else
			count_temp <= count_nxt; //If enable is active, we increment counter's value
	
end: ticks_counter		

always_comb begin // Determines if we reached overflow
	count_nxt = count_temp + 1'b1;
	if(count_nxt >= TICK_LIM)
		overflow = 1'b1;
	else
		overflow = 1'b0;
end

assign ovf = overflow;

endmodule 


